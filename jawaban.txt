/**
 *
 * @author Dian Wahyu Hudioro
 * 
 */

Jawaban :
1. CREATE DATABASE myshop;

2.
USE myshop;

CREATE TABLE IF NOT EXISTS `users` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`name` varchar( 255 ) DEFAULT NULL ,
`email` varchar( 255 ) DEFAULT NULL ,
`password` varchar( 255 ) DEFAULT NULL ,
PRIMARY KEY ( `id` )
) AUTO_INCREMENT =1;


CREATE TABLE IF NOT EXISTS `categories` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`name` varchar( 255 ) DEFAULT NULL,
PRIMARY KEY ( `id` )
) AUTO_INCREMENT =1;


CREATE TABLE IF NOT EXISTS `items` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`name` varchar( 255 ) DEFAULT NULL ,
`description` varchar( 255 ) DEFAULT NULL ,
`price` int( 11 ) DEFAULT 0,
`stock` int( 11 ) DEFAULT 0,
`category_id` int( 11 ) DEFAULT 0,
PRIMARY KEY ( `id` ),
FOREIGN KEY (`category_id`) REFERENCES categories(`id`)
) AUTO_INCREMENT =1;


3.
INSERT INTO users( name, email, PASSWORD ) VALUES ('John Doe', 'john@doe.com', 'john123');
INSERT INTO users( name, email, PASSWORD ) VALUES ('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO categories( name ) VALUES ('gadget');
INSERT INTO categories( name ) VALUES ('cloth');
INSERT INTO categories( name ) VALUES ('men');
INSERT INTO categories( name ) VALUES ('women');
INSERT INTO categories( name ) VALUES ('branded');

INSERT INTO items( name, description, price, stock, category_id ) VALUES ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1);
INSERT INTO items( name, description, price, stock, category_id ) VALUES ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2);
INSERT INTO items( name, description, price, stock, category_id ) VALUES ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


4.
SELECT id, name, email FROM `users`;
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE '%sang%';
SELECT i.name, i.description, i.price, i.stock, i.category_id, c.name AS kategori FROM items i JOIN categories c ON i.category_id = c.id;

5.
UPDATE items SET price =2500000 WHERE name = 'sumsang b50';